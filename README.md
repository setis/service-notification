## структура
````bash
.
├── app
│   ├── core.ts
│   ├── index.ts
│   ├── loaderEvents.ts
│   └── loaderView.ts
├── config.json
├── events
│   ├── index.ts
│   └── tg.ts
├── gateways
│   ├── amqp.ts
│   └── index.ts
├── index.ts
├── integrators
│   ├── index.ts
│   └── tg.ts
├── models
│   ├── index.ts
│   ├── notification.ts
│   └── token.ts
└── views
    └── index.ts
````
* gateways - шлюз принимающий данные
* integrator -  интеграция куда посылают данные
* event - событие [EventEmitter](https://nodejs.org/api/events.html)
* view - шаблоны [mustache](http://mustache.github.io/)
* model - модели данных [mongoose](http://mongoosejs.com/docs/guide.html) mongodb
````typescript
export interface iConfigure {
    gateways: {
        amqp?: {
            mode: boolean,
            params: any;
        }
    },
    integrators: {
        telegram?: {
            mode: boolean;
            params: any;
        }
    },
    model: string
}

export interface iGateway {
    connect(): Promise<void>;

    close(): Promise<void>;

    run(event: EventEmitter): Promise<void>;

    api(): any;
}

export interface iIntegrator {
    connect(): Promise<void>;

    close(): Promise<void>;

    run(event: EventEmitter): Promise<void>;

    api(): any;
}

````
## схема

gateway->event<->intergrator


## работа с контейнером(docker)
запуск rabbitmq,mongodb,nodejs,redis
````bash
docker-compose up
````
остановка
````bash
docker-compose down
````

