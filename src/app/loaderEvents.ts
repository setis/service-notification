import {join, parse, relative} from 'path'
import {lstat, readdir} from 'fs';
import {EventEmitter} from 'events';
import {promisify} from "util";


export class LoaderEvents {
    dir: string;
    event: EventEmitter = new EventEmitter();
    inject: EventEmitter;

    constructor(config: { dir: string, inject: EventEmitter, event?: EventEmitter }) {
        let {dir, inject, event = new EventEmitter()} = config;
        this.dir = dir;
        this.inject = inject;
        this.event = event;
    }

    register(path: string, module: any) {
        let {name, dir} = parse(relative(this.dir, path));
        let arr = dir.split('/');
        if (name !== 'index' && name !== undefined) {
            arr.push(name);
        }
        let exports = Object.keys(module);
        if (exports.length === 0) {
            return;
        }
        if (exports.length === 1 && typeof module.default === 'function') {
            this.inject.on(arr.join('.'), module.default);
            return;
        }
        let events = exports.filter((name) => {
            return (typeof module[name] === 'function');
        });
        if (events.length === 0) {
            return;
        }
        events.forEach(name => {
            if (name === 'default') {
                this.inject.on(arr.join('.'), module[name]);
            } else {
                this.inject.on([...arr, name].join('.'), module[name]);
            }
        });
    }

    /**
     * @description загрузка
     */
    loader(): Promise<void> {
        if (this.dir.length === 0) {
            this.event.emit('error', new Error(`не установлина директория`));
            return Promise.reject(new Error(`не установлина директория`));
        }
        this.event.emit('start');
        return this.loader_dir(this.dir)
            .then((value) => {
                this.event.emit('end');
            })
            .catch(error => {
                this.event.emit('error', error);
            });
    }

    /**
     * @description загрузка по директории
     * @param dir
     */
    loader_dir(dir: string): Promise<void> {
        return promisify(lstat)(dir)
            .then(stats => {
                if (!stats.isDirectory()) {
                    throw new Error(`не является директорией ${dir}`);
                }
            })
            .then(() => {
                return promisify(readdir)(dir)
                    .then((result) => {
                        if (result.length === 0) {
                            return Promise.resolve();
                        }
                        return Promise
                            .all(result.map(file => this.loader_file(join(dir, file))))
                            .then(() => {
                            });
                    });
            })
            .then(() => {
                this.event.emit('dir.end', dir);
            });
    }

    /**
     * @description загрузка по файлу
     * @param path
     */
    loader_file(path: string) {
        let module = require(path);
        if (typeof module.default !== 'function') {
            this.event.emit('file.fail', path);
            return Promise.resolve();
        }
        this.register(path, module);
        this.event.emit('file.success', path, module);
        return Promise.resolve();

    }
}
