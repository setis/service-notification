import {EventEmitter} from "events";
import {iConfigure, iGateway, iIntegrator} from "./index";
import {Core} from "./core";

require('mongoose').Promise = Promise;

export interface iConfigure {
    gateways: {
        amqp?: {
            mode: boolean,
            params: any;
        }
    },
    integrators: {
        telegram?: {
            mode: boolean;
            params: any;
        }
    },
    model: string
}

export interface iGateway {
    connect(): Promise<void>;

    close(): Promise<void>;

    run(event: EventEmitter): Promise<void>;

    api(): any;
}

export interface iIntegrator {
    connect(): Promise<void>;

    close(): Promise<void>;

    run(event: EventEmitter): Promise<void>;

    api(): any;
}

export * from './core';
export * from './loaderEvents';
export * from './loaderView';
export default Core