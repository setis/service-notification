import {EventEmitter} from "events";
import {normalize} from "path";
import GateWayStorage from '../gateways';
import IntegratorStorage from '../integrators';
import EventStorage from '../events';
import {iConfigure, iGateway, iIntegrator} from "./index";
import {LoaderEvents} from "./loaderEvents";
import {CoreView} from "./loaderView";
import {connect} from "mongoose";

export class Core {
    gateways: Map<string, iGateway>;
    integrators: Map<string, iIntegrator>;
    events: EventEmitter;
    config: iConfigure;
    dir: {
        [index: string]: string
    };
    view: CoreView;

    constructor(config: iConfigure) {
        this.config = config;
        this.gateways = new Map();
        this.integrators = new Map();
        this.dir = {
            gateway: normalize(`${__dirname}/../gateways/`),
            integrator: normalize(`${__dirname}/../integrators/`),
            event: normalize(`${__dirname}/../events/`),
            view: normalize(`${__dirname}/../views/`),
        };
        this.view = new CoreView(this.dir.view);
    }

    async loader(index: boolean = true) {
        await this.view.loader(index);
        await this.loaderModels();
        await this.loaderEvents(index);
        this.loaderIntegrators(index);
        this.loaderGateway(index);


    }


    async run() {
        await this.runIntegrator();
        await this.runGateway();
        // await new Promise(resolve => {
        //     setTimeout(resolve, 1e3);
        // });
    }

    protected runGateway(): Promise<void> {
        return Promise.all(
            Array
                .from(this.gateways.keys())
                .map(name => {
                    return this.gateways
                        .get(name)
                        .run(this.events)
                        .then(() => {
                            console.log(`gateway:${name} status:on`);
                        })
                        .catch((error) => {
                            console.log(`gateway:${name} status:off error:${error}`);
                        });
                }))
            .then(() => {
                this.events.emit('_loader', this);
            });
    }

    protected runIntegrator() {
        return Promise.all(
            Array
                .from(this.integrators.keys())
                .map(name => {
                    return this.integrators
                        .get(name)
                        .run(this.events)
                        .then(() => {
                            console.log(`integrator:${name} status:on`);
                        })
                        .catch((error) => {
                            console.log(`integrator:${name} status:off error:${error}`);
                        });
                }))
            .then(() => {
                this.events.emit('_loader', this);
            });
    }

    protected loaderGateway(index: boolean = true): void {
        Object.keys(this.config.gateways)
            .filter(name => {
                return (this.config.gateways[name].mode === true);
            })
            .forEach(gateway => {
                var mod;
                if (index) {
                    if (!GateWayStorage.has(gateway)) {
                        console.log(`not found gateway:${gateway}`);
                        return;
                    }
                    mod = GateWayStorage.get(gateway);
                } else {
                    try {
                        mod = require(`${this.dir.gateway}/${gateway}`).default;
                    } catch (e) {
                        console.log(`not found gateway:${gateway} error:${e.message}`);
                        return;
                    }
                }

                this.gateways.set(gateway, mod(this.config.gateways[gateway].params));

            });
    }

    protected loaderIntegrators(index: boolean = true): void {
        Object.keys(this.config.integrators)
            .filter(name => {
                return (this.config.integrators[name].mode === true);
            })
            .forEach(integrator => {
                var mod;
                if (index) {
                    if (!IntegratorStorage.has(integrator)) {
                        console.log(`not found integrator:${integrator}`);
                        return;
                    }
                    mod = IntegratorStorage.get(integrator);
                } else {
                    try {
                        mod = require(`${this.dir.integrator}/${integrator}`).default;
                    } catch (e) {
                        console.log(`not found integrator:${integrator} error:${e.message}`);
                        return;
                    }
                }
                this.integrators.set(integrator, mod(this.config.integrators[integrator].params));

            });
    }

    protected loaderEvents(index: boolean = true): Promise<void> {
        if (index) {
            this.events = EventStorage;
            return Promise.resolve();
        }
        this.events = new EventEmitter();
        this.events.setMaxListeners(0);
        let loader = new LoaderEvents({dir: this.dir.event, inject: this.events});
        return loader.loader();
    }

    protected loaderModels(): Promise<void> {
        return connect(this.config.model, {useMongoClient: true, mongos: true})
            .then(() => {
                require('../models/index');
            });

    }
}