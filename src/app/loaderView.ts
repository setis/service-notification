import {parse, render} from 'mustache'
import ListViews from '../views';
import {promisify} from "util";
import {readdir, readFile, stat} from "fs";
import {join, relative} from "path";

const loaderFile = promisify(readFile);
const loaderDir = promisify(readdir);
const fsStat = promisify(stat);

export class CoreView {
    templates: Map<string, string>;
    dir: string;

    constructor(dir: string) {
        this.templates = new Map();
        this.dir = dir;
    }

    loader(index: boolean = false): Promise<void> {
        if (index) {
            return Promise
                .all(
                    ListViews.map(file => {
                        return this.loaderFile(file)
                    }))
                .then(() => {
                });
        }
        return this.loaderDir(this.dir).then(files => {
            return Promise
                .all(files.map(file => {
                    return this.loaderFile(file)
                }))
                .then(() => {
                });
        });

    }

    loaderDir(dir: string): Promise<string[]> {
        return loaderDir(dir)
            .then(paths => {
                let files = [];
                let dirs = [];
                return Promise
                    .all(paths
                        .filter(path => {
                            return (path !== '..' && path !== '.' && path !== 'index.ts' && path !== 'index.js');
                        })
                        .map(path => {
                            return fsStat(path)
                                .then((stat) => {
                                    if (stat.isFile()) {
                                        files.push(join(dir, path));
                                    }
                                    if (stat.isDirectory()) {
                                        dirs.push(join(dir, path));
                                    }
                                });
                        }))
                    .then(() => {
                        return Promise
                            .all(dirs.map(dir => {
                                return this.loaderDir(dir);
                            }))
                            .then(dirs => {
                                return files.concat(...dirs);
                            });
                    });
            });
    }

    loaderFile(path: string): Promise<void> {
        return loaderFile(path)
            .then((data) => {
                this.templates.set(relative(this.dir, path), data.toString());
            });
    }


    register(name, template) {
        parse(template);
        this.templates.set(name, template);
    }

    render(path: string, data: any): string {
        if (!this.templates.has(path)) {
            throw new Error(`not found view:${path}`);
        }
        return render(this.templates.get(path), data)
    }

}