import {Core} from "./app";

const config = require('./config.json');
const app = new Core(config);
export default app;

app.loader()
    .then(() => {
        return app.run()
    })
    .then(() => {
        console.log(`app success`);
    })
    .catch(reason => {
        console.log(`app error:${reason}`);
    });
