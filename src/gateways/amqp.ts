import {Channel, connect, Connection} from 'amqplib';
import {hostname} from 'os';
import {EventEmitter} from "events";
import * as PromiseBlueBird from 'bluebird'

const QUEUE = `${hostname()}:${process.pid}:notice`;
const EXCHANGE = `service.notice`;
const ROUTE = `service.notice.*`;

export class GateWayAmqp {
    connection: Connection;
    channel: Channel;
    reconnect: {
        interval: number;
        try: number;
        mode: boolean;
    };
    try?: number;
    url: string;
    id: number | null;
    isConnection: boolean = false;

    constructor(config: {
        url: string, reconnect?: {
            mode: boolean;
            interval: number;
            try: number;
        }
    }) {
        let {url, reconnect = {interval: 5e3, try: 0, mode: true}} = config;
        this.url = url;
        this.reconnect = reconnect;
        if (reconnect.mode) {
            this.try = 0;
        }
        process.on('exit', this.close.bind(this));
        this.connect();

    }

    connect(): Promise<void> {
        this.id = null;
        return new Promise(resolve => {
            connect(this.url).then(value => {
                this.isConnection = true;
                this.connection = value;
                this.connection.on('error', (error) => {
                    this.isConnection = false;
                    console.log(`amqp error:${error}`);
                    this.connect();
                });
                return resolve();
            }).catch(error => {
                this.isConnection = false;
                console.log(`amqp error:${error}`);
                if (this.reconnect.mode) {
                    if (this.reconnect.try > 0) {
                        this.try++;
                        if (this.try > this.reconnect.try) {
                            console.log(`перевышил лимит попыток`);
                            process.exit();
                            return;
                        }
                    }
                    this.id = setTimeout(this.connect.bind(this), this.reconnect.interval);
                }
                return resolve();
            })
        });

    }

    close() {
        if (this.id) {
            clearTimeout(this.id);
            this.id = null;
        }
        if (this.isConnection) {
            this.channel.unbindQueue(QUEUE, EXCHANGE, ROUTE);
            this.connection.close();
        }
    }

    api():Channel{
        return this.channel;
    }

    run(event: EventEmitter): Promise<void> {

        return new Promise(async (resolve) => {
            if (this.isConnection) {
                return resolve();
            }
            let id;
            id = setInterval(() => {
                if (this.isConnection) {
                    clearInterval(id);
                    return resolve();
                }
            }, 1e3).unref();
        })
            .then(() => {
                return new Promise((resolve, reject) => {
                    this.connection
                        .createChannel()
                        .then(value => {
                            this.channel = value;
                            resolve();
                        })
                        .catch(reject)
                });
            })
            .then(async () => {
                if (false === await this.check()) {
                    await this.install();
                }
            })
            .then(() => {
                return this.channel.bindQueue(QUEUE, EXCHANGE, ROUTE);
            })
            .then(() => {
                this.channel.consume(QUEUE, (msg) => {
                    console.log(msg);
                    // let id = msg.properties.correlationId;
                    let func = msg.fields.routingKey;
                    let params = JSON.parse(msg.content.toString());
                    event.emit(func, params);
                });
            });
    }

    check(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            PromiseBlueBird
                .all([
                    this.channel.assertExchange(EXCHANGE, 'topic', {durable: false}),
                    this.channel.assertQueue(QUEUE, {durable: false, exclusive: true})
                ])
                .then((value) => {
                    resolve((value.filter(value2 => value2).length === 0));
                })
                .catch(reject);
        });
    }

    install(): Promise<void> {
        return new Promise((resolve, reject) => {
            PromiseBlueBird
                .all([
                    this.channel.assertExchange(EXCHANGE, 'topic', {durable: false}),
                    this.channel.assertQueue(QUEUE, {durable: false, exclusive: true}),

                ])
                .then(() => {
                    resolve();
                })
                .catch(reject);
        });
    }

    unistall(): Promise<void> {
        return new Promise((resolve, reject) => {
            PromiseBlueBird
                .all([
                    this.channel.deleteExchange(EXCHANGE),
                    this.channel.deleteExchange(QUEUE),
                ])
                .then(() => {
                    resolve();
                })
                .catch(reject);
        });
    }


}

export default function (config: {
    url: string, reconnect?: {
        mode: boolean;
        interval: number;
        try: number;
    }
}): GateWayAmqp {
    return new GateWayAmqp(config);
}