import amqp from './amqp';

const storage: Map<string,Function> = new Map();
export default storage;
storage.set('amqp', amqp);
