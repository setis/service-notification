import TokenModel from "../models/token";
import {Schema} from "mongoose";
import NotificationModel from "../models/notification";

const {ObjectId} = Schema.Types;
import TelegramBot = require("node-telegram-bot-api");
import {Core} from "../app/core";


let tg: TelegramBot;

export function _loader(app:Core) {
    if (tg !== undefined ||  !app.integrators.has('tg')) {
        return;
    }
    tg = app.integrators.get('tg').api();
}

export function start(id: string, key?: string) {
    if (key) {
        return TokenModel.findOne({_id: new ObjectId(key), type: 'tg'}).then(res => {
            if (res === null) {
                return tg.sendMessage(id, `не вверный токен ${key} введите токен`);
            }

            return NotificationModel
                .findOne({user: res.user})
                .then(res2 => {
                    if (res2 === null) {
                        return NotificationModel.create({
                            user: res.user,
                            intergrations: {
                                tg: id
                            }
                        });
                    }
                    return NotificationModel.findByIdAndUpdate(res2._id, {intergrations: {$set: {tg: id}}})
                })
                .then(() => {
                    return tg.sendMessage(id, `успешно введен токен`);
                });
        });
    }
    return tg.sendMessage(id, `введите токен`);
}

export function stop(id: string) {
    return NotificationModel
        .findOne({user: Number(id.trim()), "disable": {$nin: ['tg']}})
        .then(res => {
            if (res === null) {
                return tg.sendMessage(id, `успешно отключенна подписка`);
            }
            return NotificationModel.findOneAndUpdate({_id: res._id}, {$addToSet: {disable: 'tg'}}).then(res2 => {
                return tg.sendMessage(id, `успешно отключенна подписка`);
            });
        });
}
