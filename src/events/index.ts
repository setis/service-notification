import {EventEmitter} from "events";
import {start, stop,_loader} from "./tg";

const event = new EventEmitter();
export default event;
event
    .setMaxListeners(0)
    .on('tg.start', start)
    .on('tg.stop', stop)
    .on('_loader', _loader);