import TelegramBot = require('node-telegram-bot-api');
import {EventEmitter} from "events";
import {iIntegrator} from "../app/index";

export class IntegratorTelegram implements iIntegrator {


    token: string;
    _api?: TelegramBot;
    event: EventEmitter;
    _send: any;

    constructor(token: string) {
        this.token = token;

    }

    run(event: EventEmitter): Promise<void> {
        this.event = event;

        return this.connect().then(() => {
            if (!this._send) {
                this._send = this._api.sendMessage.bind(this);
            }
            this.event
                .on('tg.send', this._send);
        });
    }

    connect(): Promise<void> {
        this._api = new TelegramBot(this.token, {polling: true});
        this._api
            .onText(/\/start(.*?)$/g, (msg, match) => {
                console.log();
                this.event.emit('tg.start', msg.chat.id, (match[2] === '')?false:match[2]);
            });

        this._api
            .onText(/\/stop/, (msg) => {
                this.event.emit('tg.stop', msg.chat.id);
            });
        return Promise.resolve();
    }

    api(): TelegramBot {
        return this._api;
    }

    close(): Promise<void> {
        if (this._send) {
            this.event
                .removeListener('tg.send', this._send);
        }
        if (this.api) {
            this._api.closeWebHook();
        }
        this.api = null;
        return Promise.resolve();
    }

}

export default function (token: string): IntegratorTelegram {
    return new IntegratorTelegram(token);
}
