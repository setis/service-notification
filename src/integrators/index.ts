import IntegratorTelegram from "./tg";

const storage: Map<string, Function> = new Map();
export default storage;
storage.set('tg', IntegratorTelegram);