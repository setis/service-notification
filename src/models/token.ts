import {Document, model, Model, Schema} from "mongoose";

const TokenSchema: Schema = new Schema({
    date: Date,
    user: Number,
    type: String
});

export interface iTokenModel extends Document {
    date: Date;
    user: number;
    type: string;
}


export const TokenModel: Model<iTokenModel> = model<iTokenModel>("token", TokenSchema);
export default TokenModel;