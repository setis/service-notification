import {Document, Model, model, Schema} from "mongoose";

export const NotificationSchema: Schema = new Schema({
    user: {type: Number, unique: true, required: true},
    intergrations: {
        tg: String
    },
    disable: [{ type: String, lowercase: true, trim: true }],

});

export interface iNotificationModel extends Document {
    user: number;
    intergrations: {
        tg?: string;
    }
    disable: string[]

}

export const NotificationModel: Model<iNotificationModel> = model<iNotificationModel>("nofitication", NotificationSchema);
export default NotificationModel;